#pragma once
#include "globals.h"

// This is a class used for incrementing ticks/frames and retrieving how many seconds passed
// since the previous tick.
namespace gas
{
	class GOLDEN_API DeltaTimer
	{
		/***** Public Interface *****/
	public:
		real	Tick();					// tick, and get the new dt
		real	ElapsedSeconds() const;	// get the dt of the last tick
		void	Reset();

		/***** Init *****/
	public:
		DeltaTimer();
		~DeltaTimer();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		DeltaTimer(const DeltaTimer&) = delete;
		DeltaTimer& operator=(const DeltaTimer&) = delete;
	};
}