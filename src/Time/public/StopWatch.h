#pragma once
#include "globals.h"

// This is a class for timing the duration of something in seconds.
namespace gas
{
	class GOLDEN_API StopWatch
	{
		/***** Public Interface *****/
	public:
		real ElapsedSeconds() const;	// returns the last recorded elapsed time
		void StartNow();

		/***** Init *****/
	public:
		StopWatch();
		~StopWatch();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		StopWatch(const StopWatch&) = delete;
		StopWatch& operator=(const StopWatch&) = delete;
	};
}