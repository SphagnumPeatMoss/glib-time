#pragma once
#include "globals.h"

namespace gas
{
	class GOLDEN_API FixedTimer
	{
		/***** Public Interface *****/
	public:
		bool TryTick();
		uint64_t ElapsedTicks() const;

		void SetFixedStop(const uint timestep, const uint maxStepDiff = 5);
		void ResetTime();	// Resets the difference between last and curr time to 0

		/***** Init *****/
	public:
		FixedTimer(const uint timestep = 50, const uint maxStepDiff = 5);
		~FixedTimer();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		FixedTimer(const FixedTimer&) = delete;
		FixedTimer& operator=(const FixedTimer&) = delete;
	};
}