#pragma once
#include "globals.h"
#include <chrono>

namespace gas
{
	typedef std::chrono::high_resolution_clock			Clock;
	typedef Clock::time_point							TimePoint;
	typedef std::chrono::seconds						Seconds;
	typedef std::chrono::duration<real, std::ratio<1>>	RealDuration;
	typedef std::chrono::milliseconds					Milliseconds;
}