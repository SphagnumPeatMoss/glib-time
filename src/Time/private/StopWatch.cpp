#include "StopWatch.h"
#include "forward.h"

namespace gas
{
	struct StopWatch::Impl
	{
		TimePoint		startTime;
	};

	real StopWatch::ElapsedSeconds() const
	{
		return RealDuration(Clock::now() - m->startTime).count();
	}
	void StopWatch::StartNow()
	{
		m->startTime = Clock::now();
	}
	StopWatch::StopWatch()
		: m(make_unique<Impl>())
	{
		StartNow();
	}
	StopWatch::~StopWatch()
	{
	}
}