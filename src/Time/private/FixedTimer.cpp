#include "FixedTimer.h"
#include "forward.h"

namespace gas
{
	struct FixedTimer::Impl
	{
		TimePoint		lastTime;			// When was the last time we ticked?
		TimePoint		currTime;			// What is the current time?
		Milliseconds	timeStep;			// How long to wait between ticks
		Milliseconds	maxStepDiff;		// If dt between ticks is more than this, bring lastTime up to date
		Milliseconds	timeSinceLastTick;	// How long since the last tick increment?
		uint64_t		tick;				// current tick

		Impl(const uint _timestep, const uint _maxStepDiff)
			: timeStep(_timestep)
			, maxStepDiff(_maxStepDiff)
			, timeSinceLastTick(0)
			, tick(0)
		{

		}
	};

	bool FixedTimer::TryTick()
	{
		// Compute the change in time since last tick
		m->currTime = Clock::now();
		m->timeSinceLastTick = std::chrono::duration_cast<Milliseconds>(m->currTime - m->lastTime);

		// If we've hit or exceeded the minimum time to wait between ticks
		if (m->timeSinceLastTick >= m->timeStep)
		{
			// Increment the ticks
			++m->tick;

			// If we exceeded the max amount of time we can wait between ticks
			if (m->timeSinceLastTick > m->maxStepDiff)
			{
				// Move last time forward
				m->lastTime = m->currTime - m->maxStepDiff;
			}

			// Add the fixed timestep to the last time
			m->lastTime += m->timeStep;
			return true;
		}
		else
		{
			return false;
		}
	}
	uint64_t FixedTimer::ElapsedTicks() const
	{
		return m->tick;
	}
	void FixedTimer::SetFixedStop(const uint _timestep, const uint _maxStepDiff /*= 5*/)
	{
		m->timeStep = static_cast<Milliseconds>(_timestep);
		m->maxStepDiff = static_cast<Milliseconds>(_maxStepDiff * m->timeStep);
	}
	void FixedTimer::ResetTime()
	{
		m->lastTime = m->currTime = Clock::now() - m->timeStep;
	}
	FixedTimer::FixedTimer(const uint _timestep /*= 50*/, const uint _maxStepDiff /*= 5*/)
		: m(make_unique<Impl>(_timestep, _maxStepDiff))
	{
		ResetTime();
	}
	FixedTimer::~FixedTimer()
	{
	}
}