#include "DeltaTimer.h"
#include "forward.h"

namespace gas
{
	struct DeltaTimer::Impl
	{
		TimePoint	lastTime;
		TimePoint	currTime;
		real		dt;
	};

	real DeltaTimer::Tick()
	{
		m->currTime = Clock::now();						// Update current time
		m->dt = RealDuration(m->currTime - m->lastTime).count();	// Compute the change in time
		m->lastTime = m->currTime;							// Update last time to current

		return m->dt;
	}
	real DeltaTimer::ElapsedSeconds() const
	{
		return m->dt;
	}
	void DeltaTimer::Reset()
	{
		m->lastTime = m->currTime = Clock::now();
	}
	DeltaTimer::DeltaTimer()
		: m(make_unique<Impl>())
	{
		Reset();
	}
	DeltaTimer::~DeltaTimer()
	{
	}
}